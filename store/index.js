import vue from 'vue'
import vuex from "vuex"
import user from "./user.js"
 vue.use(vuex)
 let store = new vuex.Store({
		modules:{
		user
		}
	})
export default store