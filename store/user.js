import $http from "../untils/request"
export default {
		namespaced:true,
		state:{
			token:''
		},
		mutations:{
			savetoken(state,num){
				state.token = num
			}
		},
		actions:{
			//单机登录，触发
			savetokenRequest(context,info){
				$http('/1.1/login',info,'POST').then(res=>{
					console.log(res,'token信息');
					let {code} = res
					if(code){
						let title = code === 211 ? '账号不存在' : '密码错误'
						uni.showToast({
							title,
							icon:'none'
						})
						return
					}
					context.commit('savetoken',res)
					uni.navigateBack(1)
					uni.setStorage({
						key: 'token',
						data: res,
						success: function () {
							console.log('success');
						}
					});
				})
				// console.log(123456,info);
			},
			//点击注册时触发
			regintion(context,info){
				$http('/1.1/users',info,'POST').then(res=>{
					let {objectId,code} = res
					let title = code === 202 ? '账号已被占用' : '注册成功'
					uni.showToast({
						title,
						icon:'none'
					})
				})
			}
		}
	}