import App from './App'
import './font/iconfont.css'

// #ifndef VUE3
import Vue from 'vue'
import uView from "uview-ui";
import  $http  from "./untils/request"
import Nav from "components/Nav.vue"
import Hometitle from "components/Hometitle.vue"
import Goodtime from "components/Goodtime.vue"
import store from "./store/index.js"
Vue.use(uView);
Vue.config.productionTip = false
App.mpType = 'app'
Vue.prototype.$http = $http
Vue.prototype.$store = store
Vue.component('Nav',Nav)
Vue.component('Hometitle',Hometitle)
Vue.component('Goodtime',Goodtime)
const app = new Vue({
    ...App,
	store
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif