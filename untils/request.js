let baseURL = 'https://api2105.h5project.cn'
let $http = function(url,data={},method='GET'){
	return new Promise((resolve,reject)=>{
		uni.request({
			url:baseURL +url,
			method,
			header:{
				"X-LC-Id": "7yoqQxyLBNN9MW5rsgTGXbHL-gzGzoHsz",
				"X-LC-Key": "hA8yGYuHjnXWRui1rzTe0C3P",
				"Content-Type": "application/json"
			},
			data,
			success(res) {
				resolve(res.data)
			},
			fail(err) {
				reject(err)
			}
		})
	})
}
export default $http